# The challenge

PhysioNet Computing in Cardiology Challenge 2017: Atrial fibrillation detection from a short single lead ECG recording

https://physionet.org/challenge/2017/

The official results:

https://physionet.org/challenge/2017/results.csv

Our best result is 0.77.

# The team

University of Valencia: Computational Multiscale Simulation Lab (CoMMLab).

Team members:

- Miguel Lozano
- Viktor Kifer
- Francisco Martinez-Gil